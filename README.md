# Typescraper - Get the quotes we've always needed

Borne of me being annoyed that [wpm](https://github.com/cslarsen/wpm/) had so
many quotes and we only had about 400 or so.

Typescraper takes all of the quotes from typeracerdata.com and puts them into
the typeracerdata folder in a structure that is almost ready to go for
consumption as a Typeracer
[lang pack](https://gitlab.com/ttyperacer/lang-packs).

## Running

Binaries are not included. You must have rust installed.

Run:

```
cargo run --release -- -u http://typeracerdata.com/texts\?texts\=full\&sort\=difficulty_rating
```

URL can be specified in the case you are hosting your own typeracerdata
somewhere else.

## Notice

This repo exists purely for educational purposes to show how it would look if
typeracer the project were to have all of the quotes from typeracerdata.

All quotes are from typeracerdata.com and subject to the terms of use from
typeracerdata.com
