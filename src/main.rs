use clap::{App, Arg};
use snailquote::unescape;
use soup::{prelude::*, Soup};
use std::collections::HashMap;
use std::error::Error;
use std::fs::{create_dir_all, File};
use std::io::prelude::*;
use std::path::PathBuf;
use ureq::get;
use uuid::Uuid;

#[derive(Debug, PartialEq, Eq, Hash)]
enum Difficulty {
    Easy,
    Medium,
    Difficult,
    VeryDifficult,
}

#[derive(Debug)]
struct QuoteData {
    pub text: String,
    pub difficulty: Difficulty,
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("Typescraper, the typescraper")
        .arg(
            Arg::with_name("url")
                .short("u")
                .long("url")
                .takes_value(true)
                .required(true)
                .help("Typeracerdata.com compatible URL to scrape"),
        )
        .get_matches();

    let output =
        get_webpage_as_string(matches.value_of("url").expect("Why do you betray me clap"))?;
    write_to_data_dirs(get_quotes_from_site(&output));
    Ok(())
}

fn write_to_data_dirs(datas: Vec<QuoteData>) {
    let mut path_map: HashMap<Difficulty, PathBuf> = HashMap::new();
    path_map.insert(Difficulty::Easy, PathBuf::from("./typeracerdata/easy"));
    path_map.insert(Difficulty::Medium, PathBuf::from("./typeracerdata/medium"));
    path_map.insert(
        Difficulty::Difficult,
        PathBuf::from("./typeracerdata/difficult"),
    );
    path_map.insert(
        Difficulty::VeryDifficult,
        PathBuf::from("./typeracerdata/very_difficult"),
    );

    create_quote_dirs(&path_map);
    for data in datas {
        write_quote_to_file(&data, &path_map);
    }
}

fn create_quote_dirs(path_map: &HashMap<Difficulty, PathBuf>) {
    create_dir_all(&path_map[&Difficulty::Easy]).expect("could not create easy folder");
    create_dir_all(&path_map[&Difficulty::Medium]).expect("could not create easy folder");
    create_dir_all(&path_map[&Difficulty::Difficult]).expect("could not create easy folder");
    create_dir_all(&path_map[&Difficulty::VeryDifficult]).expect("could not create easy folder");
}

fn write_quote_to_file(data: &QuoteData, path_map: &HashMap<Difficulty, PathBuf>) {
    let mut file =
        File::create(path_map[&data.difficulty].join(Uuid::new_v4().to_hyphenated().to_string()))
            .expect("unable to write file :(");

    file.write_all(format!("{}\n{}", data.text, "typeracerdata").as_bytes())
        .expect("Could not write to file :(");
}

fn get_quotes_from_site(site: &str) -> Vec<QuoteData> {
    let mut quote_vec = vec![];
    let soup = Soup::new(site);
    for tbody in soup
        .tag("table")
        .find()
        .expect("could not find main table :(")
        .children()
        .skip(1)
    {
        for tr in tbody.children().skip(1) {
            let mut text = "".to_owned();
            let mut difficulty = 0.0;
            for (idx, td) in tr.children().enumerate() {
                if idx == 5 {
                    text = td.tag("a").find().expect("could not find a :(").text();

                    if text == "[Unknown]" {
                        break;
                    }
                } else if idx == 11 {
                    difficulty = td.text().parse::<f64>().expect("Could not parse out float");
                } else {
                    continue;
                }
            }
            if text == "" || text == "[Unknown]" {
                continue;
            } else {
                quote_vec.push(QuoteData {
                    text,
                    difficulty: difficulty_from_num(difficulty),
                })
            }
        }
    }

    quote_vec
}

fn get_webpage_as_string(url: &str) -> Result<String, Box<dyn Error>> {
    let req = get(url).call();
    Ok(req.into_string()?)
}

fn difficulty_from_num(num: f64) -> Difficulty {
    if 2.0 > num && num > 1.20 {
        Difficulty::Easy
    } else if 1.1999 > num && num > 1.0 {
        Difficulty::Medium
    } else if 0.9999 > num && num > 0.850 {
        Difficulty::Difficult
    } else {
        Difficulty::VeryDifficult
    }
}
